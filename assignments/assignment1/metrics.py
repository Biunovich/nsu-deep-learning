import numpy as np
def binary_classification_metrics(prediction, ground_truth):
    '''
    Computes metrics for binary classification

    Arguments:
    prediction, np array of bool (num_samples) - model predictions
    ground_truth, np array of bool (num_samples) - true labels

    Returns:
    precision, recall, accuracy, f1 - classification metrics
    '''
    precision = 0
    recall = 0
    accuracy = 0
    f1 = 0

    # TODO: implement metrics!
    # Some helpful links:
    # https://en.wikipedia.org/wiki/Precision_and_recall
    # https://en.wikipedia.org/wiki/F1_score
    confusion_matrix = np.zeros((2,2))
    confusion_matrix[0,0] = sum(prediction & ground_truth)
    confusion_matrix[1,1] = sum(~(prediction ^ ground_truth)) - confusion_matrix[0,0]
    for i in range(len(prediction)):
        if ((prediction[i] == True) & (ground_truth[i] == False)):
            confusion_matrix[0,1] += 1
        if ((prediction[i] == False) & (ground_truth[i] == True)):
            confusion_matrix[1,0] += 1
    print(confusion_matrix)
    # for (i in range(len(prediction))):
    #     if (prediction[i] == gro)
    # confusion_matrix[0,0] = 
    precision = confusion_matrix[0,0]/(confusion_matrix[0,0] + confusion_matrix[0,1])
    recall = confusion_matrix[0,0]/(confusion_matrix[0,0] + confusion_matrix[1,0])
    f1 = 2*(precision*recall)/(precision + recall)
    accuracy = (confusion_matrix[0,0] + confusion_matrix[1,1])/len(prediction)

    
    return precision, recall, f1, accuracy


def multiclass_accuracy(prediction, ground_truth):
    '''
    Computes metrics for multiclass classification

    Arguments:
    prediction, np array of int (num_samples) - model predictions
    ground_truth, np array of int (num_samples) - true labels

    Returns:
    accuracy - ratio of accurate predictions to total samples
    '''
    # TODO: Implement computing accuracy
    return sum(prediction == ground_truth)/len(prediction)
