import numpy as np

from layers import softmax, FullyConnectedLayer, ReLULayer, softmax_with_cross_entropy, l2_regularization


class TwoLayerNet:
    """ Neural network with two fully connected layers """

    def __init__(self, hidden_layer_size, reg):
        """
        Initializes the neural network

        Arguments:
        hidden_layer_size, int - number of neurons in the hidden layer
        reg, float - L2 regularization strength
        """
        self.reg = reg
        # TODO Create necessary layers
        self.hidden_layer1 = FullyConnectedLayer(3072, hidden_layer_size)
        self.ReLULayer = ReLULayer()
        self.hidden_layer2 = FullyConnectedLayer(hidden_layer_size, 10)

    def compute_loss_and_gradients(self, X, y):
        """
        Computes total loss and updates parameter gradients
        on a batch of training examples

        Arguments:
        X, np array (batch_size, input_features) - input data
        y, np array of int (batch_size) - classes
        """
        # TODO Compute loss and fill param gradients
        # by running forward and backward passes through the model

        # After that, implement l2 regularization on all params
        # Hint: use self.params()
        Y = self.hidden_layer1.forward(X)
        Z = self.ReLULayer.forward(Y)
        E = self.hidden_layer2.forward(Z)
        loss, probs = softmax_with_cross_entropy(E, y)
        lossRegW1, gradW1 = l2_regularization(self.params()['W1'].value, self.reg)
        lossRegW2, gradW2 = l2_regularization(self.params()['W2'].value, self.reg)
        loss += (lossRegW1 + lossRegW2)
        d_E = self.hidden_layer2.backward(probs)
        self.hidden_layer2.params()['W'].grad += gradW2
        d_Z = self.ReLULayer.backward(d_E)
        d_Y = self.hidden_layer1.backward(d_Z)
        self.hidden_layer1.params()['W'].grad += gradW1
        return loss

    def predict(self, X):
        """
        Produces classifier predictions on the set

        Arguments:
          X, np array (test_samples, num_features)

        Returns:
          y_pred, np.array of int (test_samples)
        """
        # TODO: Implement predict
        # Hint: some of the code of the compute_loss_and_gradients
        # can be reused
        pred = np.zeros(X.shape[0], np.int)
        Y = self.hidden_layer1.forward(X)
        Z = self.ReLULayer.forward(Y)
        E = self.hidden_layer2.forward(Z)
        pred = np.argmax(E, axis=1)

        return pred

    def params(self):
        result = {'W1': self.hidden_layer1.params()['W'], 'B1': self.hidden_layer1.params()['B'],
                  'W2': self.hidden_layer2.params()['W'], 'B2': self.hidden_layer2.params()['B']}


        # TODO Implement aggregating all of the params

        return result
